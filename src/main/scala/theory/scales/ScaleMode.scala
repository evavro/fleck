package theory.scales

import theory.{Note, Key, Accidental}

trait ScaleMode {

  def name: String
  
  def pattern: String
  
  def tonicRel: Int

  // T = Tone, S = Semitone
  def validatePattern = {
  	if(pattern.length != 7)
  		throw new Exception("Scale mode patterns must be 7 characters long")
  	
  	if(!pattern.forall(c => "TS".contains(c)))
  		throw new Exception("Scale mode patterns can only consist of tones (\"T\") and semitones (\"S\")")
  	
  	true
  }
  
  // take in accidental here
  def applyPattern(root: Note): List[Note] = {
    validatePattern
    
    var notes: List[Note] = Nil
    var nextNote: Note = root
    val baseAccidental = new Key(root, this).baseAccidental
    
    for(i <- pattern) {
    	notes ::= nextNote

    	val inc = i match {
    	  case 'S' => 1
    	  case 'T' => 2
    	}

    	// FIXME - could check if the note is an accidental here instaed of internally (eliminate need for fixAccidental, assumptions are bad)
    	// TODO - Use Key.fitToNote here to tighten relations
    	nextNote = new Note((nextNote.value + inc) % 12, baseAccidental)  // WARN - % 12 should probably be made optional (base on octaves?)
    }
    
    notes.reverse // FIXME - eliminate the need for this
  }
  
  def isCompatibleWith[T >: ScaleMode](mode: T): Boolean = false
  
  override def toString = name
  
}
