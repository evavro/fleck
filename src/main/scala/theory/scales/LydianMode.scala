package theory.scales

case object LydianMode extends ScaleMode {
  
  def name     = "Lydian"
  def pattern  = "TTTSTTS"
  def tonicRel = 4

}