package theory.scales

case object MajorMode extends ScaleMode {
  
  def name     = "Major / Ionian"
  def pattern  = "TTSTTTS"
  def tonicRel = 1
  
}