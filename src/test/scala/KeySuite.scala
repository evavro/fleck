package test.scala

import theory.{Key, Note}
import theory.Notes._
import theory.scales._

import org.specs2.mutable._


class KeySpec extends Specification {

  "A key" should {
    "be valid" in {
      Key.isValid("Cb") must beTrue
      Key.isValid("ab") must beTrue
      Key.isValid("Gb") must beTrue
      Key.isValid("eb") must beTrue
      Key.isValid("Db") must beTrue
      Key.isValid("bb") must beTrue
      Key.isValid("Ab") must beTrue
      Key.isValid("f")  must beTrue
      Key.isValid("Eb") must beTrue
      Key.isValid("c")  must beTrue
      Key.isValid("Bb") must beTrue
      Key.isValid("g")  must beTrue
      Key.isValid("F")  must beTrue
      Key.isValid("d")  must beTrue
      Key.isValid("C")  must beTrue
      Key.isValid("a")  must beTrue
      Key.isValid("G")  must beTrue
      Key.isValid("e")  must beTrue
      Key.isValid("D")  must beTrue
      Key.isValid("b")  must beTrue
      Key.isValid("A")  must beTrue
      Key.isValid("f#") must beTrue
      Key.isValid("E")  must beTrue
      Key.isValid("c#") must beTrue
      Key.isValid("B")  must beTrue
      Key.isValid("g#") must beTrue
      Key.isValid("F#") must beTrue
      Key.isValid("d#") must beTrue
      Key.isValid("C#") must beTrue
      Key.isValid("a#") must beTrue
    }

    "error if invalid" in {
      ("Z": Key) should throwA[IllegalArgumentException]
    }

    "return the correct number of accidentals for major keys" in {
      ("Cb": Key).numAccidentals mustEqual -7
      ("Gb": Key).numAccidentals mustEqual -6
      ("Db": Key).numAccidentals mustEqual -5
      ("Ab": Key).numAccidentals mustEqual -4
      ("Eb": Key).numAccidentals mustEqual -3
      ("Bb": Key).numAccidentals mustEqual -2
      ("F": Key).numAccidentals  mustEqual -1
      ("C": Key).numAccidentals  mustEqual 0
      ("G": Key).numAccidentals  mustEqual 1
      ("D": Key).numAccidentals  mustEqual 2
      ("A": Key).numAccidentals  mustEqual 3
      ("E": Key).numAccidentals  mustEqual 4
      ("B": Key).numAccidentals  mustEqual 5
      ("F#": Key).numAccidentals mustEqual 6
      ("C#": Key).numAccidentals mustEqual 7
    }

    "return the correct number of accidentals for minor keys" in {
      ("ab": Key).numAccidentals mustEqual -7
      ("eb": Key).numAccidentals mustEqual -6
      ("bb": Key).numAccidentals mustEqual -5
      ("f": Key).numAccidentals  mustEqual -4
      ("c": Key).numAccidentals  mustEqual -3
      ("g": Key).numAccidentals  mustEqual -2
      ("d": Key).numAccidentals  mustEqual -1
      ("a": Key).numAccidentals  mustEqual 0
      ("e": Key).numAccidentals  mustEqual 1
      ("b": Key).numAccidentals  mustEqual 2
      ("f#": Key).numAccidentals mustEqual 3
      ("c#": Key).numAccidentals mustEqual 4
      ("g#": Key).numAccidentals mustEqual 5 // BORKED
      ("d#": Key).numAccidentals mustEqual 6 // BORKED
      ("a#": Key).numAccidentals mustEqual 7 // BORKED
    }

    "imply the major mode if the key is uppercase" in {
      ("Cb": Key).mode mustEqual MajorMode
      ("Gb": Key).mode mustEqual MajorMode
      ("Db": Key).mode mustEqual MajorMode
      ("Ab": Key).mode mustEqual MajorMode
      ("Eb": Key).mode mustEqual MajorMode
      ("Bb": Key).mode mustEqual MajorMode
      ("F": Key).mode  mustEqual MajorMode
      ("C": Key).mode  mustEqual MajorMode
      ("G": Key).mode  mustEqual MajorMode
      ("D": Key).mode  mustEqual MajorMode
      ("A": Key).mode  mustEqual MajorMode
      ("E": Key).mode  mustEqual MajorMode
      ("B": Key).mode  mustEqual MajorMode
      ("F#": Key).mode mustEqual MajorMode
      ("C#": Key).mode mustEqual MajorMode
    }

    "imply the minor mode if the key is lowercase" in {
      ("ab": Key).mode mustEqual MinorMode
      ("eb": Key).mode mustEqual MinorMode
      ("bb": Key).mode mustEqual MinorMode
      ("f": Key).mode  mustEqual MinorMode
      ("c": Key).mode  mustEqual MinorMode
      ("g": Key).mode  mustEqual MinorMode
      ("d": Key).mode  mustEqual MinorMode
      ("a": Key).mode  mustEqual MinorMode
      ("e": Key).mode  mustEqual MinorMode
      ("b": Key).mode  mustEqual MinorMode
      ("f#": Key).mode mustEqual MinorMode
      ("c#": Key).mode mustEqual MinorMode
      ("g#": Key).mode mustEqual MinorMode // BORKED
      ("d#": Key).mode mustEqual MinorMode // BORKED
      ("a#": Key).mode mustEqual MinorMode // BORKED
    }

    "generate the correct list of sharp accidentals" in {
      ("C": Key).accidentals  mustEqual Nil
      ("G": Key).accidentals  mustEqual List[Note]("F#")
      ("D": Key).accidentals  mustEqual List[Note]("F#", "C#")
      ("A": Key).accidentals  mustEqual List[Note]("F#", "C#", "G#")
      ("E": Key).accidentals  mustEqual List[Note]("F#", "C#", "G#", "D#")
      ("B": Key).accidentals  mustEqual List[Note]("F#", "C#", "G#", "D#", "A#")
      ("F#": Key).accidentals mustEqual List[Note]("F#", "C#", "G#", "D#", "A#", "E#")
    }

    "generate the correct list of flat accidentals" in {
      ("F": Key).accidentals  mustEqual List[Note]("Bb")
      ("Bb": Key).accidentals mustEqual List[Note]("Bb", "Eb")
      ("Eb": Key).accidentals mustEqual List[Note]("Bb", "Eb", "Ab")
      ("Ab": Key).accidentals mustEqual List[Note]("Bb", "Eb", "Ab", "Db")
      ("Db": Key).accidentals mustEqual List[Note]("Bb", "Eb", "Ab", "Db", "Gb")
      ("Gb": Key).accidentals mustEqual List[Note]("Bb", "Eb", "Ab", "Db", "Gb", "Cb")
    }

    "fit notes to itself (key of C)" in {
      val testKey: Key = "C"
        
      // Cb is borked - do we care? is it ok to imply that it's a B? need to refresh on this concept, but probably not ok..
      testKey.fitNote("C#") mustEqual C
      testKey.fitNote("C")  mustEqual C
      testKey.fitNote("Db") mustEqual D
      testKey.fitNote("D#") mustEqual D
      testKey.fitNote("D")  mustEqual D
      testKey.fitNote("Eb") mustEqual E
      testKey.fitNote("E")  mustEqual E
      testKey.fitNote("F#") mustEqual F
      testKey.fitNote("F")  mustEqual F
      testKey.fitNote("Gb") mustEqual G
      testKey.fitNote("G#") mustEqual G
      testKey.fitNote("G")  mustEqual G
      testKey.fitNote("Ab") mustEqual A
      testKey.fitNote("A#") mustEqual A
      testKey.fitNote("A")  mustEqual A
      testKey.fitNote("Bb") mustEqual B
      testKey.fitNote("B")  mustEqual B
    }

    // testing these all isn't necessary, maybe just the two extremes (C and Gb/F#)
    /*"fit notes to itself (key of G)"
    "fit notes to itself (key of D)"
    "fit notes to itself (key of A)"
    "fit notes to itself (key of E)"
    "fit notes to itself (key of B)"
    "fit notes to itself (key of Gb/F#)"
    "fit notes to itself (key of Db)"
    "fit notes to itself (key of Ab)"
    "fit notes to itself (key of Eb)"
    "fit notes to itself (key of Bb)"
    "fit notes to itself (key of F)"*/


    // "contains note?"
    // "relative major" http://en.wikipedia.org/wiki/Relative_key
    // "relative minor" http://en.wikipedia.org/wiki/Relative_key
    // "get major keys"
    // "get minor keys"
    // "fit note to key"
  }

}