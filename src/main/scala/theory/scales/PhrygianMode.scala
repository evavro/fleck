package theory.scales

case object PhrygianMode extends ScaleMode {
  
  def name     = "Phrygian"
  def pattern  = "STTTSTT"
  def tonicRel = 3

}