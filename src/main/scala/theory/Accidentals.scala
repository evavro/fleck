package theory

// 1 = #, -1 = b, 0 = neutral
class Accidental(var value: Int) {
  
  def symbol = Accidental.toSymbol(this)
  
  def fixToNote(noteStr: String): Int = noteStr match {
    case x if Note.isFlat(noteStr) => -1
    case x if Note.isSharp(noteStr) => 1
    case _ => 0
  }
  
  override def toString = "%s".format(symbol)
  
}

object Accidental {
  
  implicit def fromInt(value: Int): Accidental = value match {
    case -1 | 0 | 1 => new Accidental(value)
    case _ => throw new Exception("Invalid accidental value: %s".format(value))
  }
  
  implicit def fromSymbol(symbol: Char): Accidental = symbol match {
    case '#' => 1
    case 'b' => -1
    case '_' => 0
    case _ => throw new Exception("Invalid accidental symbol: %s".format(symbol))
  }
  
  implicit def toInt(acc: Accidental): Int = toSymbol(acc) // multiple implicit conversions avoid code duplication
  
  implicit def toSymbol(acc: Accidental): Char = acc.value match {
    case 1 => '#'
    case -1 => 'b'
    case 0 => '_'
  }
  
}

case object Sharp extends Accidental(1)
case object Flat extends Accidental(-1)
case object Neutral extends Accidental(0)


// TODO
// IDEA - accept an implicit value that defines the preference for accidentals (# or b)
trait ScopedAccidental[A <: Accidental] {

  // FIXME - use reflection here instead of having to define get
  def get: A

}


object ScopedAccidentals {

  implicit object ScopedSharp extends ScopedAccidental[Sharp.type] {

    def get = Sharp
  
  }

  implicit object ScopedFlat extends ScopedAccidental[Flat.type] {

    def get = Flat
  
  }

}


