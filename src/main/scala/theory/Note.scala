package theory

// TODO support octave values
// IDEA - accept an implicit value that defines the preference for accidentals (# or b)

case class Note(var value: Int = 0, var accidental: Accidental = '_') { // (implicit val key: Key) <- infer accidental from this?
  
	fixValue(value)
	fixAccidental
	
	if(!Note.isValidNote(value))
	  throw new Exception("Invalid note value: %s".format(value))
  
	def this(noteName: String) = this(Note.strToInt(noteName))
	
	// Prevents negative values from seeping through - FIXME, not octave safe!
	def fixValue(newVal: Int) = { this.value = if(newVal < 0) ((12 - newVal.abs) % 12).abs else newVal }
	def fixAccidental = { this.accidental = accidental.fixToNote(toString) } // baseVal
  
	def baseVal = Note.toBaseVal(value)
	def octave  = value / 12
	
	def isAccidental: Boolean = Note.isAccidental(baseVal)
	def isSharp: Boolean = Note.isSharp(toString)
	def isFlat: Boolean  = Note.isFlat(toString)
	
	def withSharp = Note.sharps(baseVal)
	def withFlat  = Note.flats(baseVal)
	def asNatural = Note.toBaseStr(this)

	def +(inc: Int): Note = Note(value + inc)
	def -(dec: Int): Note = Note(value - dec)

	// FIXME - return new immutable instance
	def +=(inc: Int): Note = { fixValue(value + inc); this }
	def -=(dec: Int): Note = { fixValue(value - dec); this }

	// Determines the difference in values (offset) between two notes
	def -(other: Note): Int = value - other.value
	
	def ==(other: Note): Boolean = baseVal == other.baseVal
	
	// Determines if an accidental is within 1 range (e.g., C -> C# or D -> Db)
	def ~==(other: Note): Boolean = value match {
	  case x if (x == (other.value + 1) || x == (other.value - 1)) => true
	  case _ => false
	}

	// Determines the normalized pitch distance between two notes
	def <->(other: Note): Int = {
	  val distance = other.value - value
	  val distNorm = if (distance < (-12 / 2)) 12 else 0

	  (distance + distNorm) % 12
	}

	// Determines the bi-directional normalized pitch distance between two notes (reflexive)
	def <~>(other: Note): Int = {
	  val normDistance = this <-> other

	  if (normDistance < 0) normDistance + 12 else normDistance 
	}

	// get rid of this
	def str = toString

	override def equals(that: Any) = that match {
      case n: Note => this == n
      case _ => false
    }
	
	override def toString = accidental.symbol match {
	  case '#' => withSharp
	  case 'b' => withFlat
	  case _   => Note.intToStr(value)
	}
	
	implicit def strAsInt(note: String): Int = Note.strToInt(note)
	
}

object Note {

	def bases  = Map("C" -> 0, "D" -> 2, "E" -> 4, "F" -> 5, "G" -> 7, "A" -> 9, "B" -> 11)
	def all    = Vector("C", "C#/Db", "D", "D#/Eb", "E", "F", "F#/Gb", "G", "G#/Ab", "A", "A#/Bb", "B")
	def sharps = Vector("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B")
	def flats  = Vector("C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B")
	def fifths = Vector("F", "C", "G", "D", "A", "E", "B")
	
	def isValidNote(note: String): Boolean = {
	  if(!bases.contains(toBaseStr(note)))
	    return false
	  	
	  for(accidental <- getAccidentals(note))
	    if(!Note.isValidAccidental(accidental))
	      return false
	    	
	  true
	}
	
	def isAccidental(baseNoteVal: Int): Boolean = {
	  for((k,v) <- Note.bases) {
		if(v == baseNoteVal)
		  return false
	  }
	  true
	}
	
	def isSharp(note: String): Boolean = note.contains("#") && !note.contains("b")
	def isFlat (note: String): Boolean = note.contains("b") && !note.contains("#")
	def hasAccidental(note: String): Boolean = isSharp(note) || isFlat(note)
	
	def isValidNote(note: Int): Boolean = isValidNote(intToStr(note))
	def isValidAccidental(acc: Char): Boolean = acc match {
	  case '#' | 'b' => true
	  case _ => false
	}
	
	def toBaseStr(note: String): String = note.charAt(0).toString.toUpperCase
	def toBaseVal(note: Int): Int = if (note < all.length) note else note % 12
	
	def getAccidentals(note: String): Array[Char] = note.substring(1, note.length).toCharArray()
	def getAccidental(note: String): Accidental = {
	  val allAccidentals = getAccidentals(note)

	  if(allAccidentals.length > 0) allAccidentals.head else '_'
	}
	
	// TODO - Figure out if it's possible to use implicit here with two parameters, Implement octaves
	// FIXME - get rid of default accidental, take in an implicit!
	def intToStr(num: Int, acc: Accidental = '#'): String = {
	  if(num < 0)
	    throw new Exception("Note values must be positive: %s".format(num))
	  
	  val base = toBaseVal(num)
	  
	  return acc.symbol match {
	    case '#' => sharps(base)
	    case 'b' => flats(base)
	    case _   => throw new Exception("Invalid accidental, must be # or b")
	  }
	}
	
	def strToInt(note: String): Int = {
	  val base: String = toBaseStr(note)
	  var value: Int = 0

	  if(isValidNote(note))
	    value = bases(base)
	  else
	    throw new IllegalArgumentException("Unrecognized note: " + note)

	  for(acc <- getAccidentals(note)) {
	    value = acc match {
	      case '#' => value + 1
	      case 'b' => value - 1
	      case _   => value
	    }
	  }
	  
	  value
	}
	
	implicit def fromInt(value: Int): Note = new Note(value)
	implicit def fromStr(note: String): Note = hasAccidental(note) match {
	  case true  => new Note(strToInt(note), getAccidental(note))
	  case false => new Note(strToInt(note))
	}
	
	implicit def toInt(note: Note): Int = note.value
	implicit def toStr(note: Note): String = note.toString
	
}

object Notes {

	def Cb = Note(-1, 'b')
	def C  = Note(0)
	def C$ = Note(1, '#')
	def Db = Note(1, 'b')
	def D  = Note(2)
	def D$ = Note(3, '#')
	def Eb = Note(3, 'b')
	def E  = Note(4)
	def E$ = Note(5, '#')
	def Fb = Note(4, 'b')
	def F  = Note(5)
	def F$ = Note(6, '#')
	def Gb = Note(6, 'b')
	def G  = Note(7)
	def G$ = Note(8, '#')
	def Ab = Note(8, 'b')
	def A  = Note(9)
	def A$ = Note(10, '#')
	def Bb = Note(10, 'b')
	def B  = Note(11)
	def B$ = Note(12, '#')

}
