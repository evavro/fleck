package theory.scales

case object MixolydianMode extends ScaleMode {
  
  def name     = "Mixolydian"
  def pattern  = "TTSTTST"
  def tonicRel = 5

}