package theory.chords

import theory.Note
import theory.Note._
import theory.Notes._
import theory.Intervals._

// http://www.musictheory.net/lessons/43

sealed trait Triad {

  val tonic: Note
  val mediant: Note
  val dominant: Note
  val label: String

  def asTuple = (tonic, mediant, dominant)

  override def toString = s"${tonic}${label}"

}

case class MajorTriad(t: Note) extends Triad {

  val tonic    = t
  val mediant  = t + M3
  val dominant = t + P5
  val label    = "M"

}

case class MinorTriad(t: Note) extends Triad {

  val tonic    = t
  val mediant  = t + m3
  val dominant = t + P5
  val label    = "m"

}

case class DiminishedTriad(t: Note) extends Triad {

  val tonic    = t
  val mediant  = t + m3
  val dominant = t + d5
  val label    = "dim"

}

case class AugmentedTriad(t: Note) extends Triad {

  val tonic    = t
  val mediant  = t + M3
  val dominant = t + A5
  val label    = "+"

}

case class Sus2Triad(t: Note) extends Triad {

  val tonic    = t
  val mediant  = t + M2
  val dominant = t + P5
  val label    = "sus2"

}

case class Sus4Triad(t: Note) extends Triad {

  val tonic    = t
  val mediant  = t + P4
  val dominant = t + P5
  val label    = "sus4"

}

case class UnidentifiedTriad(t: Note, m: Note, d: Note) extends Triad {

  val tonic    = t
  val mediant  = m
  val dominant = d
  val label    = "?"

}

object Triad {

  def apply(tonic: Note, mediant: Note, dominant: Note): Triad = {
    (tonic <~> mediant, tonic <~> dominant) match {
      case (`M3`, `P5`) => MajorTriad(tonic)
      case (`m3`, `P5`) => MinorTriad(tonic)
      case (`m3`, `d5`) => DiminishedTriad(tonic)
      case (`M3`, `A5`) => AugmentedTriad(tonic)
      case (`M2`, `P5`) => Sus2Triad(tonic)
      case (`P4`, `P5`) => Sus4Triad(tonic)
      case _            => UnidentifiedTriad(tonic, mediant, dominant)
    }
  }

  implicit def fromNoteSeq(notes: Seq[Note]): Triad = {
    if (notes.length.equals(3)) {
      Triad(notes(0), notes(1), notes(2))
    } else {
      throw new TriadStateError
    }
  }

  //implicit def fromStr(str: String): Triad

}

case class TriadStateError extends IllegalStateException("Triads consist of exactly 3 notes")
case class TriadIdentificationError extends UnsupportedOperationException("Failed to identify triad type")