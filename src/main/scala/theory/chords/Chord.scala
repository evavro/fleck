package theory.chords

import theory.Note

// TODO - determine if triad
// abstract class Chord(notes: Note*)

// take in triad type (?)
abstract class Chord(label: String = "C", voicingId: Int = 0, interval: Int = 8) {
  
	// TODO parse label
	def getRoot: Note = label
	
	def getNotes: List[Note] = Nil
	
	def getRelated: List[Chord] = Nil
	
}

// TODO - Convert these to triad objects
class MajorChord(label: String, voicingId: Int) extends Chord(label, voicingId) {
  
}

class MinorChord(label: String, voicingId: Int) extends Chord(label, voicingId) {
  
}

class AugmentedChord(label: String, voicingId: Int) extends Chord(label, voicingId) {
  
}

class DiminishedChord(label: String, voicingId: Int) extends Chord(label, voicingId) {
  
}

class Sus2Chord(label: String, voicingId: Int) extends Chord(label, voicingId) {

}

class Sus4Chord(label: String, voicingId: Int) extends Chord(label, voicingId) {

}

/*object ChordBuilder {

 	def apply(notes: Note*) = {

 	}

}*/

object Chord {
  
	def I(root: Note) = null
	def II(root: Note) = null
	def III(root: Note) = null
	def IV(root: Note) = null
	def V(root: Note) = null
	def VI(root: Note) = null
	def VII(root: Note) = null
	def VIII(root: Note) = null
  
	def parseLabel(label: String):Chord = {
	  val root: String = Note.toBaseStr(label)
	  // change 
	  //val triad: Triad = label
	  // Maj
	  // Min
	  // Dim
	  // Aug
	  null
	}
  
}