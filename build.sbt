// Set the project name to the string 'My Project'
name := "Fleck Music Theory"
 
// The := method used in Name and Version is one of two fundamental methods.
// The other method is <<=
// All other initialization methods are implemented in terms of these.
version := "1.0"
 
//Add Repository Path
//resolvers += "db4o-repo" at "http://source.db4o.com/maven"
 
// Add a single dependency
libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.1.3"

libraryDependencies += "org.specs2" %% "specs2" % "2.2.2" % "test"