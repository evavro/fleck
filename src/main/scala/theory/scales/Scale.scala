package theory.scales

import theory.Note
import theory.Accidental
import theory.Key

// Diatonic scale

class Scale(root: Note = "C", mode: ScaleMode = MajorMode) {

  // FIXME determine the key of the scale based on the root note (look in Key.keys)
  // FIXME - octaves is borked, very strange behavior. it's not idempotent either, even though we aren't working with mutable objects. freaky.
  
  // Take in accidental option
  def getNotes(octaves: Int = 1): List[Note] = { 
    var notes = List[Note]()
    
    for(i <- 0 until octaves)
      notes :::= mode.applyPattern(root += (i * octaves))
    
    notes
  }
  
  def noteInScale(note: Note): Boolean = getNotes().contains(note)
  def noteAt(degree: Int): Note = getNotes()(degree)
  
  def getKey: Key = new Key(root, mode)
  def getTriads = null
  
  // Degrees
  
  def tonic = noteAt(0)
  def supertonic = noteAt(1)
  def mediant = noteAt(2)
  def subdominant = noteAt(3)
  def dominant = noteAt(4)
  def submediant = noteAt(5)
  def leadingTone = noteAt(6)

}

object Scale {
  
  def baseScale = List("C", "D", "E", "F", "G", "A", "B")
  
}

case class MajorScale(root: Note) extends Scale(root, MajorMode)
case class MinorScale(root: Note) extends Scale(root, MinorMode)

