package theory.scales

case object MinorMode extends ScaleMode {
  
  def name     = "Natural Minor / Aeolian"
  def pattern  = "TSTTSTT"
  def tonicRel = 6

}