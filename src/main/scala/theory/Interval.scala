package theory

// http://en.wikipedia.org/wiki/Interval_(music)

object Intervals {

	val P1 = 0
	val d2 = 0
	val m2 = 1
	val A1 = 1
	val M2 = 2
	val d3 = 2
	val m3 = 3
	val A2 = 3
	val M3 = 4
	val d4 = 4
	val P4 = 5
	val A3 = 5
	val A4 = 6
	val d5 = 6
	val P5 = 7
	val d6 = 7
	val m6 = 8
	val A5 = 8
	val M6 = 9
	val d7 = 9
	val m7 = 10
	val A6 = 10
	val M7 = 11
	val d8 = 11
	val P8 = 12
	val A7 = 12
  
}