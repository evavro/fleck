package theory

import theory.scales._


// TODO Take in an optional parameter for selecting key by # of accidental (- = sharp, + = flat)
// FIXME String is taken in here as opposed to note in order to prevent conversion of Cb to B in Note
case class Key(key: String = "C", mode: ScaleMode = MajorMode) {

  def name = "%s [%s]".format(key, mode)
  
  // FIXME - G does not turn into Gb in key of Db
  // example, take in F, turn into F# if key of D
  def fitNote(note: Note): Note = accidentals match {
    case Nil => note.asNatural
    case _ => {
      for(acc <- accidentals) {
        if(acc.asNatural == note.asNatural)
          return new Note(note.value + (acc - note), baseAccidental)
      }
      note
    }
  }
  
  // TODO - Return an ordered list of the notes in this natural key.
  // getNotes('F') -> ['F', 'G', 'A', 'Bb', 'C', 'D', 'E']
  def getNotes: List[Note] = {
    var notes: List[Note] = Nil
    val keyNote: Note = key
    
    for(i <- 0 until 7)
      notes :+ fitNote(keyNote += (i + 1)) // increase by whole steps
    
    return notes
  }

  // FIXME - Doesn't work for flat key signatures
  def accidentals: List[Note]    = Key.accidentals(this)
  def baseAccidental: Accidental = Key.baseAccidental(this)
  def numAccidentals: Int        = Key.numAccidentals(this, mode)
  
  def hasNote(note: Note): Boolean = getNotes.contains(note)
  
  override def toString = key

  // !!! TODO - make this visible everywhere, would be great to use in Note, ScaleMode, etc.
  implicit val defaultAccidental = baseAccidental

}

object Key {
  
  // (Major Key, Minor Key) -> Number of Accidentals
  //                           - = flat, + = sharp
  val keyCircle = Map(
    ("Cb", "Ab") -> -7,	//  b
    ("Gb", "Eb") -> -6,	//  b
    ("Db", "Bb") -> -5,	//  b
    ("Ab", "F") -> -4,	//  b
    ("Eb", "C") -> -3,	//  b
    ("Bb", "G") -> -2,	//  b
    ("F", "D") -> -1,	  //  b
    ("C", "A") -> 0,	  //  notta
    ("G", "E") -> 1,	  //  #
    ("D", "B") -> 2,	  //  #
    ("A", "F#") -> 3,	  //  #
    ("E", "C#") -> 4,	  //  #
    ("B", "G#") -> 5,	  //  #
    ("F#", "D#") -> 6,	//  #
    ("C#", "A#") -> 7 	//  #
  )
  
  def numSharps: List[Note] = List(null, "F#", "C#", "G#", "D#", "A#", "E#", "B#")
  def numFlats:  List[Note] = List(null, "Bb", "Eb", "Ab", "Db", "Gb", "Cb", "Fb")
  
  def majorKeys = for(k <- keyCircle) yield k._1._1
  def minorKeys = for(k <- keyCircle) yield k._1._2
  
  def keysByAcc(acc: Accidental): List[Note] = acc.symbol match {
    case '#' => numSharps
    case 'b' => numFlats
    case _ => Nil // C instead of Nil?
  }
  
  def isValid(key: String): Boolean = {
    for(k <- keyCircle)
      if(k._1._1 == key || k._1._2.toLowerCase == key.toLowerCase) return true

    return false
  }
  
  def validate(key: Note) = {
    if(!isValid(key))
      throw new IllegalArgumentException("Invalid key value: %s".format(key))
  }
  
  def validate(accidentals: Int) = {
    if(!(-7 until 7).contains(accidentals))
      throw new IllegalArgumentException("Invalid value for accidentals (-7 to 7 required)")
  }
  
  // FIXME - make this a try or an option
  def getKey[T >: ScaleMode](accidentals: Int, mode: T = MajorMode): Key = {
    validate(accidentals)

    for(k <- keyCircle) {
      if(k._2 == accidentals) {
        return mode match {
          case MajorMode => k._1._1
          case _ => k._1._2
      	}
      }
    }

    null
  }
  
  // FIXME - need to get list of accidentals a different way
  def accidentals(key: Key): List[Note] = {
    var notes: List[Note] = Nil
    val baseAcc: Accidental = key.baseAccidental
    val keyList: List[Note] = Key.keysByAcc(baseAcc) // key.accidental
    val numAccidentals: Int = key.numAccidentals.abs
    
    for((key,i) <- keyList.view.zipWithIndex) {
      if(i <= numAccidentals && key != null)
        notes ::= key
    }
    
    notes.reverse
  }
  
  def numAccidentals[T >: ScaleMode](key: String, mode: T = MajorMode): Int = {
    val majorMode = MajorMode
    val minorMode = MinorMode
    
    for(k <- keyCircle) {
      val maj = k._1._1
      val min = k._1._2
      val accidentals = k._2
      
      if((mode == majorMode && maj.equals(key)) || (mode == minorMode && min.toLowerCase.equals(key.toLowerCase)))
        return accidentals
    }
    
    0
  }
  
  // - = Flats, + = Sharps
  def baseAccidental(key: Key): Accidental = key.numAccidentals match {
    case x if x > 0 => '#'
    case x if x < 0 => 'b'
    case _ => '_'
  }
  
  def relativeMajor(minorKey: Key): String = {
    for(k <- keyCircle)
      if(k._1._2 == minorKey) return k._1._1

    throw new Exception("%s is not a minor key".format(minorKey))
  }
  
  def relativeMinor(majorKey: Key): Note = {
    for(k <- keyCircle)
      if(k._1._1 == majorKey) return k._1._2

    throw new Exception("%s is not a major key".format(majorKey))
  }
  
  implicit def toNote(k: Key): Note = k.key
  
  implicit def toStr(k: Key): String = k.toString
  
  implicit def fromNote(n: Note): Key = new Key(n)
  
  implicit def fromInt(i: Int): Key = Key.getKey(i)
  
  implicit def fromString(key: String): Key = {
    validate(key)
    
    for(k <- keyCircle) {
      val maj = k._1._1
      val min = k._1._2
      
      if(key == maj)
        return new Key(key, MajorMode)
      if(key == min.toLowerCase()) // allows for definition of a key in minor simply by writing it out in lowercase
        return new Key(key, MinorMode)
    }
    
    // TODO throw exception
    null
  }
  
}

