package test.scala

import theory.Note
import theory.Notes._

import org.specs2.mutable._


class NoteSpec extends Specification {

  "A note" should {
    "retain a valid base value" in {
      Note(0).baseVal  mustEqual 0
      Note(1).baseVal  mustEqual 1
      Note(2).baseVal  mustEqual 2
      Note(3).baseVal  mustEqual 3
      Note(4).baseVal  mustEqual 4
      Note(5).baseVal  mustEqual 5
      Note(6).baseVal  mustEqual 6
      Note(7).baseVal  mustEqual 7
      Note(8).baseVal  mustEqual 8
      Note(9).baseVal  mustEqual 9
      Note(10).baseVal mustEqual 10
      Note(11).baseVal mustEqual 11
      Note(12).baseVal mustEqual 0      
      Note(13).baseVal mustEqual 1
    }

    "be compared strictly by pitch value" in {
      Cb mustEqual B
      C$ mustEqual Db
      D$ mustEqual Eb
      E$ mustEqual F
      Fb mustEqual E
      F$ mustEqual Gb
      G$ mustEqual Ab
      A$ mustEqual Bb
      B$ mustEqual C
    }

    "be properly determined from a string with 0 or 1 accidentals" in {
      ("Cb": Note).value mustEqual 11
      ("C": Note).value  mustEqual 0
      ("C#": Note).value mustEqual 1
      ("Db": Note).value mustEqual 1
      ("D": Note).value  mustEqual 2
      ("D#": Note).value mustEqual 3
      ("Eb": Note).value mustEqual 3
      ("E": Note).value  mustEqual 4
      ("E#": Note).value mustEqual 5
      ("Fb": Note).value mustEqual 4
      ("F": Note).value  mustEqual 5
      ("F#": Note).value mustEqual 6
      ("Gb": Note).value mustEqual 6
      ("G": Note).value  mustEqual 7
      ("G#": Note).value mustEqual 8
      ("Ab": Note).value mustEqual 8
      ("A": Note).value  mustEqual 9
      ("A#": Note).value mustEqual 10
      ("Bb": Note).value mustEqual 10
      ("B": Note).value  mustEqual 11
      ("B#": Note).value mustEqual 12
    }

    "be properly determined from a string with more than 1 accidental" in {
      ("C##": Note)  mustEqual ("D": Note)
      ("C###": Note) mustEqual ("D#": Note)
      ("Cbb": Note)  mustEqual ("Bb": Note)
      ("Cbbb": Note) mustEqual ("A": Note)
    }

    "have an identifiable sharp accidental" in {
      Note.isSharp("C")  must beFalse
      Note.isSharp("C#") must beTrue
      Note.isSharp("D#") must beTrue
      Note.isSharp("E#") must beTrue // FIXME - E$ returns false because it gets modified to F at some point
    }

    "have an identifiable flat accidental" in {
      Note.isFlat("B")  must beFalse
      Note.isFlat("B#") must beFalse
      Note.isFlat("Cb") must beTrue
    }

    "be able to increase in pitch" in {
      var note: Note = "C"

      note += 1
      note mustEqual C$
    }

    "be able to decrease in pitch" in {
      var note: Note = "C"

      note -= 1
      note mustEqual B
    }

    "not be generated from invalid formats" in {
      ("Z": Note)   should throwA[IllegalArgumentException]
      ("123": Note) should throwA[IllegalArgumentException]
    }

    // note as int
    // note as str
  }
  
}