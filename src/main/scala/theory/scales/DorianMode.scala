package theory.scales

case object DorianMode extends ScaleMode {
  
  def name     = "Dorian"
  def pattern  = "TSTTTST"
  def tonicRel = 2
  
}