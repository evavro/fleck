package test.scala

import theory.Note
import theory.scales._

import org.scalatest._

// TODO put all of these scales into a huge sequence, then loop through and call test("")
class ScaleSuite extends FunSuite {
  
  // Major scales
  
  test("C Major") {
    assert(new MajorScale("C").getNotes() ===  List[Note]("C", "D", "E", "F", "G", "A", "B"))
  }
  
  test("G Major") {
    assert(new MajorScale("G").getNotes() === List[Note]("G", "A", "B", "C", "D", "E", "F#"))
  }
  
  test("D Major") {
    assert(new MajorScale("D").getNotes() === List[Note]("D", "E", "F#", "G", "A", "B", "C#"))
  }
  
  test("A Major") {
    assert(new MajorScale("A").getNotes() === List[Note]("A", "B", "C#", "D", "E", "F#", "G#"))
  }
  
  test("E Major") {
    assert(new MajorScale("E").getNotes() === List[Note]("E", "F#", "G#", "A", "B", "C#", "D#"))
  }
  
  test("B Major") {
    assert(new MajorScale("B").getNotes() === List[Note]("B", "C#", "D#", "E", "F#", "G#", "A#"))
  }
  
  test("F# Major") {
    assert(new MajorScale("F#").getNotes() === List[Note]("F#", "G#", "A#", "B", "C#", "D#", "E#"))
  }
  
  // FIXME - Cb
  /*test("Db Major") {
    assert(new MajorScale("Db").getNotes() === List[Note]("Db", "Eb", "F", "Gb", "Ab", "Bb", "Cb"))
  }*/
  
  test("Ab Major") {
    assert(new MajorScale("Ab").getNotes() === List[Note]("Ab", "Bb", "C", "Db", "Eb", "F", "G"))
  }
  
  test("Eb Major") {
    assert(new MajorScale("Eb").getNotes() === List[Note]("Eb", "F", "G", "Ab", "Bb", "C", "D"))
  }
  
  test("Bb Major") {
    assert(new MajorScale("Bb").getNotes() === List[Note]("Bb", "C", "D", "Eb", "F", "G", "A"))
  }
  
  // Minor scales
  
  test("A Minor") {
    assert(new MinorScale("A").getNotes() === List[Note]("A", "B", "C", "D", "E", "F", "G"))
  }
  
  test("E Minor") {
    assert(new MinorScale("E").getNotes() === List[Note]("E", "F#", "G", "A", "B", "C", "D"))
  }
  
  test("B Minor") {
    assert(new MinorScale("B").getNotes() === List[Note]("B", "C#", "D", "E", "F#", "G", "A"))
  }
  
  test("F# Minor") {
    assert(new MinorScale("F#").getNotes() === List[Note]("F#", "G#", "A", "B", "C#", "D", "E"))
  }
  
  test("C# Minor") {
    assert(new MinorScale("C#").getNotes() === List[Note]("C#", "D#", "E", "F#", "G#", "A", "B"))
  }
  
  test("G# Minor") {
    assert(new MinorScale("G#").getNotes() === List[Note]("G#", "A#", "B", "C#", "D#", "E", "F#"))
  }
  
  test("D# Minor") {
    assert(new MinorScale("D#").getNotes() === List[Note]("D#", "E#", "F#", "G#", "A#", "B", "C#"))
  }
  
  test("Bb Minor") {
    assert(new MinorScale("Bb").getNotes() === List[Note]("Bb", "C", "Db", "Eb", "F", "Gb", "Ab"))
  }
  
  test("F Minor") {
    assert(new MinorScale("F").getNotes() === List[Note]("F", "G", "Ab", "Bb", "C", "Db", "Eb"))
  }
  
  test("C Minor") {
    assert(new MinorScale("C").getNotes() === List[Note]("C", "D", "Eb", "F", "G", "Ab", "Bb"))
  }
  
  test("G Minor") {
    assert(new MinorScale("G").getNotes() === List[Note]("G", "A", "Bb", "C", "D", "Eb", "F"))
  }
  
  test("D Minor") {
    assert(new MinorScale("D").getNotes() === List[Note]("D", "E", "F", "G", "A", "Bb", "C"))
  }

}